//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
    // This is taking the colour of each pixel and passing it through to be drawn 
    gl_FragColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
    
    // Set RGB channels to 1.0 (max) and A to what's been set already
    gl_FragColor = vec4(1.0,1.0,1.0,gl_FragColor.a);
} 