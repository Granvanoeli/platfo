{
    "id": "66a87254-7125-433c-8f34-e71addaabf17",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dead",
    "eventList": [
        {
            "id": "4095c590-6977-480e-a66a-aa1df52f851e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "66a87254-7125-433c-8f34-e71addaabf17"
        },
        {
            "id": "aedc8528-a99d-4b13-9a2e-d9500c47076a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "66a87254-7125-433c-8f34-e71addaabf17"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "97cd4936-86cb-46b3-8a0a-73cf2f4f1059",
    "visible": true
}