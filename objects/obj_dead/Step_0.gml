/// @description Insert description here
// You can write your code in this editor

if(done == 0) {
    verticalSpeed = verticalSpeed + grv;

    // Horizontal collision
    if(place_meeting(x + horizontalSpeed,  y, obj_wall)){
	
    	// While it is FALSE that there would be a collision between the player and the 
    	// wall at the coordinates specified, move the player one pixel closer to the wall.
    	while(!place_meeting(x + sign(horizontalSpeed), y, obj_wall)){
    		x = x + sign(horizontalSpeed);
    	}
    	horizontalSpeed = 0;
	
    }

    x = x + horizontalSpeed;


    // Vertical collision
    if(place_meeting(x, y+verticalSpeed, obj_wall)){
	
        if (verticalSpeed > 0){ 
            done = 1;
            image_index = 1;
        }
    	// While it is FALSE that there would be a collision between the player and the 
    	// wall at the coordinates specified, move the player one pixel closer to the wall.      
    	while(!place_meeting(x, y + sign(verticalSpeed), obj_wall)){
    		y = y + sign(verticalSpeed);
    	}
    	verticalSpeed = 0;
	
    }

    y = y + verticalSpeed;

}