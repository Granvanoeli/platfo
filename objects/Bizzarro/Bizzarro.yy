{
    "id": "ab82f12c-4aef-483c-a9d0-6b16eab69e8b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Bizzarro",
    "eventList": [
        {
            "id": "1355e71a-71d8-4762-8c79-b9966ba25537",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ab82f12c-4aef-483c-a9d0-6b16eab69e8b"
        },
        {
            "id": "57ea2c78-3508-42ab-b5fc-2de8bed8a89c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ab82f12c-4aef-483c-a9d0-6b16eab69e8b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a622c91c-a13a-4e41-a396-26dc1d70c261",
    "visible": true
}