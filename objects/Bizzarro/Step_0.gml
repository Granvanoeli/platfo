/// @description Insert description here
// You can write your code in this editor
verticalSpeed = verticalSpeed + grv; //0.3

if(place_meeting(x, y+verticalSpeed, obj_wall)){
	
	// 
	// While it is FALSE that there would be a collision between the player and the 
	// wall at the coordinates specified, move the player one pixel closer to the wall.
	while(!place_meeting(x, y + sign(verticalSpeed), obj_wall)){
		
		// This is only executed when the object touches the floor (twice)
		//show_debug_message(verticalSpeed);
		y = y + sign(verticalSpeed);
	}
	verticalSpeed = 0;
	
}

y = y + verticalSpeed;
