{
    "id": "11cf300f-1b2e-4156-9952-02c06df94e97",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "4c0fb7f7-648a-4074-9a22-4cccd0f9c8b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "11cf300f-1b2e-4156-9952-02c06df94e97"
        },
        {
            "id": "a366e41b-12bc-4571-8f29-7bd1e8b8f79d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "11cf300f-1b2e-4156-9952-02c06df94e97"
        },
        {
            "id": "fc852050-4ca6-4180-8048-1e1a9f395c15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "11cf300f-1b2e-4156-9952-02c06df94e97"
        },
        {
            "id": "3ed5a6d3-75d7-46f3-a621-753408dfdb19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "11cf300f-1b2e-4156-9952-02c06df94e97"
        }
    ],
    "maskSpriteId": "2705ff7e-a278-42fd-a57b-fc48956f1059",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b4904983-c668-434b-9b7c-5ede33864dc9",
    "visible": true
}