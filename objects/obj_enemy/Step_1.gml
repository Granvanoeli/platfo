/// @description Insert description here
// You can write your code in this editor

if(hp <= 0){
	
	//Returns the exact instance of obj_dead just created
	with(instance_create_layer(x,y,layer,obj_dead)){
		direction = other.hitfrom;
		horizontalSpeed = lengthdir_x(3, direction);
		verticalSpeed = lengthdir_y(3, direction)-2
		if(sign(horizontalSpeed != 0))image_xscale = sign(horizontalSpeed);		
	};
	instance_destroy();
}