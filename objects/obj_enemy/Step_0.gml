verticalSpeed = verticalSpeed + grv;


// Horizontal collision
if(place_meeting(x + horizontalSpeed,  y, obj_wall)){
	
	// While it is FALSE that there would be a collision between the player and the 
	// wall at the coordinates specified, move the player one pixel closer to the wall.
	while(!place_meeting(x + sign(horizontalSpeed), y, obj_wall)){
		x = x + sign(horizontalSpeed);
	}
	horizontalSpeed = -horizontalSpeed;
	
}

x = x + horizontalSpeed;


// Vertical collision
if(place_meeting(x, y+verticalSpeed, obj_wall)){
	
	// While it is FALSE that there would be a collision between the player and the 
	// wall at the coordinates specified, move the player one pixel closer to the wall.
	while(!place_meeting(x, y + sign(verticalSpeed), obj_wall)){
		y = y + sign(verticalSpeed);
	}
	verticalSpeed = 0;
	
}

y = y + verticalSpeed;

// ANIMATION

// If there is no collision one pixel immediately below us
if(!place_meeting(x, y+1, obj_wall)){
	
	sprite_index = spr_enemyA;
	image_speed = 0;
	
	if(sign(verticalSpeed) > 0) image_index = 1; else image_index = 0;
}
else {
	image_speed = 1;
	
	if (horizontalSpeed == 0){
		sprite_index = spr_enemy;
	}
	else{
		sprite_index = spr_enemyR;
	}
}

// Orient the sprite to the right or to the left
if (horizontalSpeed != 0) image_xscale = sign(horizontalSpeed);