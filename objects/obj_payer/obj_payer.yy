{
    "id": "fc169499-5605-46e2-960c-d6794973ea30",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_payer",
    "eventList": [
        {
            "id": "184825a0-b2e2-4ead-b9bc-5f27f41e3b3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fc169499-5605-46e2-960c-d6794973ea30"
        },
        {
            "id": "88264764-60af-43e6-a0f4-dd4d35bbeaf0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fc169499-5605-46e2-960c-d6794973ea30"
        },
        {
            "id": "9b685183-05e3-483c-afe4-2a474185c002",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "fc169499-5605-46e2-960c-d6794973ea30"
        }
    ],
    "maskSpriteId": "2705ff7e-a278-42fd-a57b-fc48956f1059",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2705ff7e-a278-42fd-a57b-fc48956f1059",
    "visible": true
}