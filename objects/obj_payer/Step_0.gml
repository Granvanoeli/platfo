
// Get player input
key_left = keyboard_check(vk_left) || keyboard_check(ord("A"));
key_right = keyboard_check(vk_right) || keyboard_check(ord("D"));;
key_jump = keyboard_check_pressed(vk_space) || keyboard_check(ord("W")); 

if (key_left || key_right || key_jump){
	controller = 0;
}

// gamepad_axis_value returns a value between -1 and 1
if (abs(gamepad_axis_value(0, gp_axislh)) > 0.2){
	key_left = abs(min(gamepad_axis_value(0,gp_axislh), 0));
	key_right = max(gamepad_axis_value(0,gp_axislh), 0);
	controller = 1;
}

// The reason why we don't just:
// key_jump = gamepad_button_check_pressed(0, gp_face1)
// is because we don't always overwrite the kwyboard input
if (gamepad_button_check_pressed(0, gp_face1)){
	key_jump = 1;
	controller = 1;
}

// Calculate movement
var move = key_right - key_left;


horizontalSpeed = move * walkSpeed;
verticalSpeed = verticalSpeed + grv;

// If the player is on the floor and the jump key has been pressed
if(place_meeting(x, y+1, obj_wall)) && (key_jump){
	verticalSpeed = -7;
}

// Horizontal collision
if(place_meeting(x + horizontalSpeed,  y, obj_wall)){
	
	// While it is FALSE that there would be a collision between the player and the 
	// wall at the coordinates specified, move the player one pixel closer to the wall.
	while(!place_meeting(x + sign(horizontalSpeed), y, obj_wall)){
		x = x + sign(horizontalSpeed);
	}
	horizontalSpeed = 0;
	
}

x = x + horizontalSpeed;


// Vertical collision
if(place_meeting(x, y+verticalSpeed, obj_wall)){
	
	// While it is FALSE that there would be a collision between the player and the 
	// wall at the coordinates specified, move the player one pixel closer to the wall.
	while(!place_meeting(x, y + sign(verticalSpeed), obj_wall)){
		y = y + sign(verticalSpeed);
	}
	verticalSpeed = 0;
	
}

y = y + verticalSpeed;

// ANIMATION

// If there is no collision one pixel immediately below us
if(!place_meeting(x, y+1, obj_wall)){
	
	sprite_index = spr_playerA;
	image_speed = 0;
	
	if(sign(verticalSpeed) > 0) image_index = 1; else image_index = 0;
}
else {
	image_speed = 1;
	
	if (horizontalSpeed == 0){
		sprite_index = spr_player;
	}
	else{
		sprite_index = spr_playerR;
	}
}

// Orient the sprite to the right or to the left
if (horizontalSpeed != 0) image_xscale = sign(horizontalSpeed);