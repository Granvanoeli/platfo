 /// @description Insert description here
// You can write your code in this editor.

// What is in here happens every frame, before the step event

// Doing this here in the Begin Step makes it so that 
// this code is executed a little after the one for the player object.
// This creates a nice effect. Here we set the coordinates of the gun to the same X as the player and Y + 10
x = obj_payer.x;
y = obj_payer.y + 10;

// Adjust the angle of the sprite depending on mouse position (or controller)
if(obj_payer.controller ==0){
	image_angle = point_direction(x,y,mouse_x, mouse_y);
}
else {
	var controllerH = gamepad_axis_value(0, gp_axisrh);
	var controllerV = gamepad_axis_value(0, gp_axisrv);
	if (abs(controllerH) > 0.2 || abs(controllerV) > 0.2) {
		
		controllerAngle = point_direction(0, 0, controllerH, controllerV)
	}
	image_angle = controllerAngle;
}
// Reduces firingDelay by one each frame
firingDelay -= 1;
// This is a way to make sure we don't return something below 0
recoil = max(0, recoil - 1);

// Checking that firingDelay is less than 0 and that the left button of the mouse is pressed.
if ((mouse_check_button(mb_left)) || gamepad_button_check(0, gp_shoulderrb)) && (firingDelay < 0) {
    
    // Resets firingDelay
	firingDelay = 5;
    recoil = 4;
    // The with statement makes anything in between curly brackets
    // happen to an instance of the specified object. In this case obj_bullet. 
	with (instance_create_layer(x,y, "Bullets", obj_bullet)){ // Create an instance of the bullet object on the "Bullets" layer
		speed = 25;
		
		//"other" refers back to the origin al object, in this case obj_gun
		direction = other.image_angle + random_range(-3, 3);
		image_angle = direction;
	}
}

// We are subtracting the value because we want to move in the direction opposite to image_angle.
x = x - lengthdir_x(recoil, image_angle);
y = y - lengthdir_y(recoil, image_angle);

if (image_angle > 90) && (image_angle < 270) {
    image_yscale = -1;    
}

else{
 image_yscale = 1;   
}