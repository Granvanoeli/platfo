{
    "id": "b5312e61-bdb4-415e-8021-f82de4e7b739",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gun",
    "eventList": [
        {
            "id": "8a53b1a2-867a-4f6e-b188-470f4bd32ed7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "b5312e61-bdb4-415e-8021-f82de4e7b739"
        },
        {
            "id": "3d3dab19-44f8-477c-a0b2-cd6e616071c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b5312e61-bdb4-415e-8021-f82de4e7b739"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "bb900a9b-098e-405e-848b-1255d40e0b4f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "c9c7dec2-e598-4b0d-bd73-15e02d1b9289",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 28,
            "y": 0
        },
        {
            "id": "480f322e-6e81-48fd-bc55-27dae881483d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 28,
            "y": 9
        },
        {
            "id": "96ed4e79-8360-4d1b-b18d-c6a1c0fb74cb",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 9
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ada8bd05-e899-4baf-8a27-feaae57b434e",
    "visible": true
}