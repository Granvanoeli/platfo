/// @description Insert description here
// You can write your code in this editor

// We do this to make sure that the object actually gets destroyed only if it was previously drawn.
// We do this here because if we destroyed the instance in the step event, 
// we wouldn't be able to see the muzzle flash while shooting at the ground. 
 if(place_meeting(x,y,obj_wall)) instance_destroy();
