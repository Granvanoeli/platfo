{
    "id": "9b3cfe46-a3d3-4f7e-a581-eeb2910e0d41",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "82dbf2ba-0af8-4916-9f15-0c1907bc7ade",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "9b3cfe46-a3d3-4f7e-a581-eeb2910e0d41"
        },
        {
            "id": "f16d720c-fe96-47a3-acf1-a7bbdfcb2bc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "9b3cfe46-a3d3-4f7e-a581-eeb2910e0d41"
        },
        {
            "id": "49e9ed98-b05b-4a59-8f50-5ef8da7ca2b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "11cf300f-1b2e-4156-9952-02c06df94e97",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9b3cfe46-a3d3-4f7e-a581-eeb2910e0d41"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a622c91c-a13a-4e41-a396-26dc1d70c261",
    "visible": true
}