{
    "id": "2705ff7e-a278-42fd-a57b-fc48956f1059",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 18,
    "bbox_right": 30,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21e4ae4d-614c-4088-b5e6-41632e4a5fdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2705ff7e-a278-42fd-a57b-fc48956f1059",
            "compositeImage": {
                "id": "b872d803-e049-4660-9241-b500f2828a77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21e4ae4d-614c-4088-b5e6-41632e4a5fdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d5fbe4a-3ede-48ba-896e-0690d6eef4e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21e4ae4d-614c-4088-b5e6-41632e4a5fdd",
                    "LayerId": "be088112-6056-4fa6-a883-4d7a0512ed32"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "be088112-6056-4fa6-a883-4d7a0512ed32",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2705ff7e-a278-42fd-a57b-fc48956f1059",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}