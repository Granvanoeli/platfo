{
    "id": "97cd4936-86cb-46b3-8a0a-73cf2f4f1059",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 3,
    "bbox_right": 47,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a1e05e3-ed7e-43fc-89db-b9fe6069f768",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97cd4936-86cb-46b3-8a0a-73cf2f4f1059",
            "compositeImage": {
                "id": "16390b7d-c282-48c7-8e93-dbf0d2abcd6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a1e05e3-ed7e-43fc-89db-b9fe6069f768",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71550853-2ea2-4c22-8956-c57ece80d94a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a1e05e3-ed7e-43fc-89db-b9fe6069f768",
                    "LayerId": "b69d49a5-e058-47b2-9ee3-aa30bc41771e"
                }
            ]
        },
        {
            "id": "1793b1ca-fcc4-4bee-916c-f6259ee745d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97cd4936-86cb-46b3-8a0a-73cf2f4f1059",
            "compositeImage": {
                "id": "57eb4571-d8cb-421f-a844-7f2bdef36ff5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1793b1ca-fcc4-4bee-916c-f6259ee745d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e56ab807-9ac5-4386-a9e5-3ec2405675ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1793b1ca-fcc4-4bee-916c-f6259ee745d5",
                    "LayerId": "b69d49a5-e058-47b2-9ee3-aa30bc41771e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "b69d49a5-e058-47b2-9ee3-aa30bc41771e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97cd4936-86cb-46b3-8a0a-73cf2f4f1059",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 21,
    "yorig": 40
}