{
    "id": "66f271b4-f371-4579-b850-bf3de2ad7a7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemyR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "213f1162-bad5-45bc-8e46-5842eefd77d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66f271b4-f371-4579-b850-bf3de2ad7a7b",
            "compositeImage": {
                "id": "78646710-4f61-44c7-bf3d-8f0353c36d38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "213f1162-bad5-45bc-8e46-5842eefd77d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6138c52d-6105-43ed-a4a6-bf5600aac1a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "213f1162-bad5-45bc-8e46-5842eefd77d9",
                    "LayerId": "9329b3e6-0834-4e89-a8d4-686ad1c67b4e"
                }
            ]
        },
        {
            "id": "bc2f3b90-d888-4320-8f9d-eb67c0483892",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66f271b4-f371-4579-b850-bf3de2ad7a7b",
            "compositeImage": {
                "id": "d61419f5-f63f-455c-a967-0aefbf44e3d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc2f3b90-d888-4320-8f9d-eb67c0483892",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e860307-cf6d-4766-88d0-f6166042de1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc2f3b90-d888-4320-8f9d-eb67c0483892",
                    "LayerId": "9329b3e6-0834-4e89-a8d4-686ad1c67b4e"
                }
            ]
        },
        {
            "id": "16d19a72-cf09-40da-b095-5a6cd88df94b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66f271b4-f371-4579-b850-bf3de2ad7a7b",
            "compositeImage": {
                "id": "b01d8016-d97f-4fbb-89dd-73ddc336df2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16d19a72-cf09-40da-b095-5a6cd88df94b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49e352e8-6687-49b0-b7f7-a5887c0bd44b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16d19a72-cf09-40da-b095-5a6cd88df94b",
                    "LayerId": "9329b3e6-0834-4e89-a8d4-686ad1c67b4e"
                }
            ]
        },
        {
            "id": "f1b1c566-f774-4dd7-a9d5-42a7d5fd2e68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66f271b4-f371-4579-b850-bf3de2ad7a7b",
            "compositeImage": {
                "id": "e742465d-823d-4567-8598-d4a9dc30fee9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1b1c566-f774-4dd7-a9d5-42a7d5fd2e68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7104b8ec-18bb-4809-9904-a5302317d535",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1b1c566-f774-4dd7-a9d5-42a7d5fd2e68",
                    "LayerId": "9329b3e6-0834-4e89-a8d4-686ad1c67b4e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "9329b3e6-0834-4e89-a8d4-686ad1c67b4e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66f271b4-f371-4579-b850-bf3de2ad7a7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}