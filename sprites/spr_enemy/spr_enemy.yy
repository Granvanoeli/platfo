{
    "id": "b4904983-c668-434b-9b7c-5ede33864dc9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 17,
    "bbox_right": 31,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfa49370-bef3-40a9-afce-6e710d1b53f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4904983-c668-434b-9b7c-5ede33864dc9",
            "compositeImage": {
                "id": "c4fbe30c-9d99-4f10-8fd4-d84ca00f203f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfa49370-bef3-40a9-afce-6e710d1b53f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d77be68-d449-448f-8ee1-47a8d913afe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfa49370-bef3-40a9-afce-6e710d1b53f2",
                    "LayerId": "941a5e8c-45ec-46d5-9fe7-34110d72aee4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "941a5e8c-45ec-46d5-9fe7-34110d72aee4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4904983-c668-434b-9b7c-5ede33864dc9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}