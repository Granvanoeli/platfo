{
    "id": "a622c91c-a13a-4e41-a396-26dc1d70c261",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 7,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "939eb5f1-a3e3-4588-a758-169b0d3be0de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a622c91c-a13a-4e41-a396-26dc1d70c261",
            "compositeImage": {
                "id": "cadf66b7-b847-49fc-8dd9-158c1be4c872",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "939eb5f1-a3e3-4588-a758-169b0d3be0de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51af5bd1-5328-4c60-8d69-9a78e193772d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "939eb5f1-a3e3-4588-a758-169b0d3be0de",
                    "LayerId": "d137e33a-d747-4595-a46a-2617dfcdb5ec"
                }
            ]
        },
        {
            "id": "22a6cb66-7778-4255-80ff-bc057bd8d002",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a622c91c-a13a-4e41-a396-26dc1d70c261",
            "compositeImage": {
                "id": "478abe39-18f5-461d-bfc9-2226cbda1c8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22a6cb66-7778-4255-80ff-bc057bd8d002",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d84948c-da7b-4111-96fd-f5211e87b331",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22a6cb66-7778-4255-80ff-bc057bd8d002",
                    "LayerId": "d137e33a-d747-4595-a46a-2617dfcdb5ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "d137e33a-d747-4595-a46a-2617dfcdb5ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a622c91c-a13a-4e41-a396-26dc1d70c261",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 9
}