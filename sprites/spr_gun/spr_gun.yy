{
    "id": "ada8bd05-e899-4baf-8a27-feaae57b434e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5891b9d8-eed9-4e86-a799-4c3f5f7d40bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ada8bd05-e899-4baf-8a27-feaae57b434e",
            "compositeImage": {
                "id": "64f41371-d94a-4bbc-80c2-7bff3197cc30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5891b9d8-eed9-4e86-a799-4c3f5f7d40bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a8b59f7-9900-4297-aec9-669fecbf3760",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5891b9d8-eed9-4e86-a799-4c3f5f7d40bc",
                    "LayerId": "49d3f1bf-6765-4e61-baef-f03c043d1815"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "49d3f1bf-6765-4e61-baef-f03c043d1815",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ada8bd05-e899-4baf-8a27-feaae57b434e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 10,
    "yorig": 4
}