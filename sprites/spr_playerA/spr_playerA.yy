{
    "id": "39d143d9-31df-4ab9-a5e7-28b2788928e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b190fc3e-0cf3-40f0-9d5a-628be5fddc18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d143d9-31df-4ab9-a5e7-28b2788928e9",
            "compositeImage": {
                "id": "f1cd0490-33f4-42a7-a8fb-c084992dcacb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b190fc3e-0cf3-40f0-9d5a-628be5fddc18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebac61ec-7f4e-4f24-9ed4-9017c133ede1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b190fc3e-0cf3-40f0-9d5a-628be5fddc18",
                    "LayerId": "d64f7f8a-3b70-414b-aa67-f93e8f943fca"
                }
            ]
        },
        {
            "id": "74d7dc02-deca-4d02-bf08-72766c12ba1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d143d9-31df-4ab9-a5e7-28b2788928e9",
            "compositeImage": {
                "id": "548e4414-d03e-4123-85f8-d8bb1d2dcafe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74d7dc02-deca-4d02-bf08-72766c12ba1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ab0fead-c5cc-4e81-a4f5-ab075db94e19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74d7dc02-deca-4d02-bf08-72766c12ba1f",
                    "LayerId": "d64f7f8a-3b70-414b-aa67-f93e8f943fca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "d64f7f8a-3b70-414b-aa67-f93e8f943fca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39d143d9-31df-4ab9-a5e7-28b2788928e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}