{
    "id": "a6394100-9b0a-4b40-a88f-75328df0ae4e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemyA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9c36476-07ff-4539-bcf1-9fb47813180b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6394100-9b0a-4b40-a88f-75328df0ae4e",
            "compositeImage": {
                "id": "94e31329-7d00-43a3-b864-7995e52c69c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9c36476-07ff-4539-bcf1-9fb47813180b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65e65679-a528-4231-9d27-7bba96eb0c92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9c36476-07ff-4539-bcf1-9fb47813180b",
                    "LayerId": "cf4c4ca6-2e8f-4827-92cb-6db617281659"
                }
            ]
        },
        {
            "id": "c97af4cc-922a-412f-aa21-e9416ec6eb90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6394100-9b0a-4b40-a88f-75328df0ae4e",
            "compositeImage": {
                "id": "6f6bf403-3d11-4b72-ac3d-c772394f1e02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c97af4cc-922a-412f-aa21-e9416ec6eb90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4294b96-1981-43a5-bea6-d918dc82f4bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c97af4cc-922a-412f-aa21-e9416ec6eb90",
                    "LayerId": "cf4c4ca6-2e8f-4827-92cb-6db617281659"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "cf4c4ca6-2e8f-4827-92cb-6db617281659",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6394100-9b0a-4b40-a88f-75328df0ae4e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}