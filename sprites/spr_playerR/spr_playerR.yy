{
    "id": "ac85c84b-63e0-4877-b954-0e95fec73b30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c129f2e1-072a-40cb-890c-486eed48c73e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac85c84b-63e0-4877-b954-0e95fec73b30",
            "compositeImage": {
                "id": "9f74a307-767c-49f0-a3a0-e251b2b890f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c129f2e1-072a-40cb-890c-486eed48c73e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60d112a7-189b-41b7-a649-a6030418ded9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c129f2e1-072a-40cb-890c-486eed48c73e",
                    "LayerId": "f5eb5566-8700-442f-bf1f-0b39e534242b"
                }
            ]
        },
        {
            "id": "e173ee1a-267b-4bfd-a4e9-845f0c7788fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac85c84b-63e0-4877-b954-0e95fec73b30",
            "compositeImage": {
                "id": "1e4d8028-257b-4fe1-ad1d-a6d2a7e0e2fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e173ee1a-267b-4bfd-a4e9-845f0c7788fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c647b397-2bcf-4eff-9489-c05bb5cdfee0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e173ee1a-267b-4bfd-a4e9-845f0c7788fc",
                    "LayerId": "f5eb5566-8700-442f-bf1f-0b39e534242b"
                }
            ]
        },
        {
            "id": "5ff4587f-600b-491e-9c65-c8d7806f9832",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac85c84b-63e0-4877-b954-0e95fec73b30",
            "compositeImage": {
                "id": "35ed9ba8-3cd0-4b24-aecc-295e1468e1aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ff4587f-600b-491e-9c65-c8d7806f9832",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c31fac5-78ae-42cf-8a78-f009d68562c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ff4587f-600b-491e-9c65-c8d7806f9832",
                    "LayerId": "f5eb5566-8700-442f-bf1f-0b39e534242b"
                }
            ]
        },
        {
            "id": "324f3264-0798-4582-8056-941ff4cb2b15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac85c84b-63e0-4877-b954-0e95fec73b30",
            "compositeImage": {
                "id": "c963b14d-c620-4ceb-aaff-b81a10cbd64d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "324f3264-0798-4582-8056-941ff4cb2b15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e4d264b-4c8e-4d4f-ba03-72218595d933",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "324f3264-0798-4582-8056-941ff4cb2b15",
                    "LayerId": "f5eb5566-8700-442f-bf1f-0b39e534242b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "f5eb5566-8700-442f-bf1f-0b39e534242b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac85c84b-63e0-4877-b954-0e95fec73b30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}