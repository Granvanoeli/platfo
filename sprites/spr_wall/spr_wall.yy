{
    "id": "75c98097-2afb-4f86-be41-79d46aecc103",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "465f12ad-bd75-44d2-b615-1c1d2e957437",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75c98097-2afb-4f86-be41-79d46aecc103",
            "compositeImage": {
                "id": "2fc4367b-bc53-494a-aaf5-e03c18035d61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "465f12ad-bd75-44d2-b615-1c1d2e957437",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ca7ff15-4c42-4cbf-b077-93817024557c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "465f12ad-bd75-44d2-b615-1c1d2e957437",
                    "LayerId": "8e508e33-88eb-4767-95cb-04309d60c218"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8e508e33-88eb-4767-95cb-04309d60c218",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75c98097-2afb-4f86-be41-79d46aecc103",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}